package nl.capgemini.wet.microservices.frontend;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.wet.microservices.frontend.services.MoviesFrontendService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Slf4j
@Controller
@RequiredArgsConstructor
public class MoviesController {

    private final MoviesFrontendService moviesFrontendService;

    @GetMapping({"", "/"})
    public Object moviesIndex(final Model model) {
        model.addAttribute("movies", moviesFrontendService.getMoviesList());
        return "index";
    }

    @GetMapping("/{id}")
    public Object movieDetail(@PathVariable("id") final String id,final Model model) {
        try {
            model.addAttribute("movie", moviesFrontendService.getMovieDetails(Long.parseLong(id)));
            return "detail";
        } catch (final NumberFormatException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("id must be an integer");
        }
    }

}
