package nl.capgemini.wet.microservices.frontend.services;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.wet.microservices.frontend.model.ActorLink;
import nl.capgemini.wet.microservices.frontend.model.MovieDetails;
import nl.capgemini.wet.microservices.frontend.model.MovieLink;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class MoviesFrontendService {

    private final RestJsonTemplate restTemplate;
    private final Environment environment;

    public List<MovieLink> getMoviesList() {
        final ResponseEntity<List<MovieLink>> result = restTemplate.getForEntity(
                environment.getRequiredProperty("movies.data.host") + "/api/movies",
                new TypeReference<>() {
                }
        );
        if (result.getStatusCode().is2xxSuccessful()) {
            return result.getBody();
        }
        return Collections.emptyList();
    }

    public MovieDetails getMovieDetails(final long id) {
        final MovieDetails result = doGetMovieDetails(id);
        if (!ObjectUtils.isEmpty(result) && !ObjectUtils.isEmpty(result.getActorIds())) {
            Arrays.stream(result.getActorIds().split(","))
                    .map(this::doGetActorDetails)
                    .filter(Objects::nonNull)
                    .forEach(result.getActors()::add);
        }
        return result;
    }

    private ActorLink doGetActorDetails(final String actorId) {
        final ResponseEntity<ActorLink> result = restTemplate.getForEntity(
                environment.getRequiredProperty("actors.data.host") + "/api/actors/" + actorId,
                new TypeReference<>() {
                }
        );
        if (result.getStatusCode().is2xxSuccessful()) {
            return result.getBody();
        }
        return null;
    }

    private MovieDetails doGetMovieDetails(final long id) {
        final ResponseEntity<MovieDetails> result = restTemplate.getForEntity(
                environment.getRequiredProperty("movies.data.host") + "/api/movies/" + id,
                new TypeReference<>() {
                }
        );
        if (result.getStatusCode().is2xxSuccessful()) {
            return result.getBody();
        }
        return MovieDetails.builder()
                .title("Unknown")
                .uniqueId(0)
                .actors("")
                .build();
    }

}
