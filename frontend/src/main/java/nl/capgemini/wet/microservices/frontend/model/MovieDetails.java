package nl.capgemini.wet.microservices.frontend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@Jacksonized
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovieDetails {

    private final long uniqueId;
    private final String title;
    private final int releaseYear;
    @Getter(AccessLevel.NONE)
    private final String actors;
    @Getter(AccessLevel.NONE)
    private final List<ActorLink> actorLinks = new ArrayList<>();

    public String getActorIds() {
        return actors;
    }

    public List<ActorLink> getActors() {
        return actorLinks;
    }

}
