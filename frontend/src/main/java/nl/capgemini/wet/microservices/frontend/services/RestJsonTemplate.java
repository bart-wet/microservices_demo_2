package nl.capgemini.wet.microservices.frontend.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class RestJsonTemplate {

    private final RestTemplate restTemplate;
    private final ObjectMapper jsonMapper = new ObjectMapper();

    public RestJsonTemplate() {
        this(new RestTemplateBuilder()
                .errorHandler(new CustomErrorHandler())
                .build()
        );
    }

    @SneakyThrows
    public <T> ResponseEntity<T> getForEntity(final String url, final TypeReference<T> responseType) {
        try {
            final ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

            if (response.getStatusCode().is2xxSuccessful()) {
                return new ResponseEntity<>(
                        jsonMapper.readValue(response.getBody(), responseType),
                        response.getHeaders(),
                        response.getStatusCode()
                );
            } else {
                log.warn("Response {}: {}", response.getStatusCode(), response.getBody());
                return new ResponseEntity<>(
                        null,
                        response.getHeaders(),
                        response.getStatusCode()
                );
            }
        } catch (final Exception e) {
            log.warn("Failed to connect to {}", url);
            return new ResponseEntity<>(
                    null,
                    new HttpHeaders(),
                    HttpStatus.SERVICE_UNAVAILABLE
            );
        }
    }


    private static class CustomErrorHandler extends DefaultResponseErrorHandler {

        @Override
        protected void handleError(final ClientHttpResponse response, final HttpStatus statusCode) {
            // Do nothing error codes
        }

    }

}
