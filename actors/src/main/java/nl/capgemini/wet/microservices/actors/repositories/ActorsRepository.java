package nl.capgemini.wet.microservices.actors.repositories;

import nl.capgemini.wet.microservices.actors.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActorsRepository extends JpaRepository<Actor, Long> {

    Optional<Actor> findByUniqueId(long uniqueId);

}
