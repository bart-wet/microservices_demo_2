package nl.capgemini.wet.microservices.actors;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.wet.microservices.actors.model.Actor;
import nl.capgemini.wet.microservices.actors.services.ActorDataService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/actors")
public class ActorDataController {

    private final ActorDataService actorDataService;

    @GetMapping({"", "/"})
    public List<Actor> getMovieList() {
        return actorDataService.getAllMovies();
    }

    @GetMapping("/{uniqueId}")
    public ResponseEntity<Actor> getMovie(
            @PathVariable("uniqueId") final long uniqueId
    ) {
        return actorDataService.getActor(uniqueId)
                .map(m -> ResponseEntity.status(HttpStatus.OK).body(m))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

}
