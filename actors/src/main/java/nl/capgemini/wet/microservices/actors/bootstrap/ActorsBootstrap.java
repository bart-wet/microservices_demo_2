package nl.capgemini.wet.microservices.actors.bootstrap;

import lombok.RequiredArgsConstructor;
import nl.capgemini.wet.microservices.actors.model.Actor;
import nl.capgemini.wet.microservices.actors.repositories.ActorsRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Configuration
@RequiredArgsConstructor
public class ActorsBootstrap {

    private final ActorsRepository repository;

    @EventListener
    public void createMovies(final ContextRefreshedEvent event) {
        repository.save(makeActor("Tim allen", 920));
        repository.save(makeActor("Sigourney Weaver", 922));
        repository.save(makeActor("Alan Rickman", 925));
        repository.save(makeActor("Jackie Chan", 1024));
        repository.save(makeActor("Michelle Ferre", 1070));
        repository.save(makeActor("Ron Smoorenburg", 182));
    }

    private Actor makeActor(final String name, final int uniqueId) {
        final Actor result = new Actor();
        result.setUniqueId(uniqueId);
        result.setName(name);
        return result;
    }

}
