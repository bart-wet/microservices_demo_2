package nl.capgemini.wet.microservices.actors.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.wet.microservices.actors.model.Actor;
import nl.capgemini.wet.microservices.actors.repositories.ActorsRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ActorDataService {

    private final ActorsRepository repository;

    public List<Actor> getAllMovies() {
        return repository.findAll();
    }

    public Optional<Actor> getActor(final long uniqueId) {
        return repository.findByUniqueId(uniqueId);
    }

}
