package nl.capgemini.wet.microservices.movies.repositories;

import nl.capgemini.wet.microservices.movies.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MoviesRepository extends JpaRepository<Movie, Long> {

    Optional<Movie> findByUniqueId(long uniqueId);

}
