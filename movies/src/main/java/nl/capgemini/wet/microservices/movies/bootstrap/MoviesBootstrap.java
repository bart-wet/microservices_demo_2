package nl.capgemini.wet.microservices.movies.bootstrap;

import lombok.RequiredArgsConstructor;
import nl.capgemini.wet.microservices.movies.model.Movie;
import nl.capgemini.wet.microservices.movies.repositories.MoviesRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Configuration
@RequiredArgsConstructor
public class MoviesBootstrap {

    private final MoviesRepository repository;

    @EventListener
    public void createMovies(final ContextRefreshedEvent event) {
        repository.save(makeMovie("Galaxy Quest", 918, 1999, "920,922,925"));
        repository.save(makeMovie("Who Am I?", 924, 1999, "1024,1070,182"));
    }

    private Movie makeMovie(final String title, final int uniqueId, final int year, final String actors) {
        final Movie result = new Movie();
        result.setUniqueId(uniqueId);
        result.setTitle(title);
        result.setReleaseYear(year);
        result.setActors(actors);
        return result;
    }

}
