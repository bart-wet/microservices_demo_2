package nl.capgemini.wet.microservices.movies.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.wet.microservices.movies.model.Movie;
import nl.capgemini.wet.microservices.movies.repositories.MoviesRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MovieDataService {

    private final MoviesRepository repository;

    public List<Movie> getAllMovies() {
        return repository.findAll();
    }

    public Optional<Movie> getMovie(final long uniqueId) {
        return repository.findByUniqueId(uniqueId);
    }

}
