package nl.capgemini.wet.microservices.movies;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.wet.microservices.movies.model.Movie;
import nl.capgemini.wet.microservices.movies.services.MovieDataService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/movies")
public class MoviesDataController {

    private final MovieDataService movieDataService;

    @GetMapping({"", "/"})
    public List<Movie> getMovieList() {
        return movieDataService.getAllMovies();
    }

    @GetMapping("/{uniqueId}")
    public ResponseEntity<Movie> getMovie(
        @PathVariable("uniqueId") final long uniqueId
    ) {
        return movieDataService.getMovie(uniqueId)
                .map(m -> ResponseEntity.status(HttpStatus.OK).body(m))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

}
