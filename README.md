# Microservices Demo 2

This project demonstrates microservice orchestration with 2 separated databases and 1 frontend services using Spring 
boot.

## Requirements

- Java 11+
- IDE
- Maven

## How to run

Open the project in your IDE and import all 3 maven projects.

Start the `ActorsApplication` main class
Start the `MoviesApplication` main class
Start the `FrontendApplication` main class

Open http://localhost:8080 in a browser.

## Infrastructure

The `FrontendApplication` calls the other 2 applications for its data using HTTP. Both the `MoviesApplication` and 
`ActorsApplication` each contain a database and bootstrapped data.
The `FrontendApplication` has some error handling if the other applications are unreachable, but this can be improved
by adding a resiliency framework or sidecar.  

## Knowledge to understand it better

This application is build using Spring Boot with H2 databases. To really understand the application knowledge of Spring
Boot and deeper understanding of Dependency Injection and Java is necessary.
